package br.com.mastertech.contatos.controllers;


import br.com.mastertech.contatos.DTOs.ContatoDTO;
import br.com.mastertech.contatos.converters.ContatoConverter;
import br.com.mastertech.contatos.models.Contato;
import br.com.mastertech.contatos.security.Usuario;
import br.com.mastertech.contatos.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping ("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private ContatoConverter converter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContatoDTO criarContato(@RequestBody @Valid Contato contato, @AuthenticationPrincipal Usuario usuario){
        Contato contatodb = contatoService.criarContato(contato, usuario);

        return converter.toContatoDTO(contatodb);
    }

    @GetMapping
    public List<ContatoDTO> consultarContatos(@AuthenticationPrincipal Usuario usuario){
        List<Contato> listaContatosdb = contatoService.consultarContatos(usuario);

        return converter.toContatosDTO(listaContatosdb);
    }
}
