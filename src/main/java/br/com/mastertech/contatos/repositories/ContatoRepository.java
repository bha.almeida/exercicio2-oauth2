package br.com.mastertech.contatos.repositories;

import br.com.mastertech.contatos.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository <Contato, Integer> {

    List<Contato> findAllByIdUsuario(int idUsuario);
}
