package br.com.mastertech.contatos.converters;

import br.com.mastertech.contatos.DTOs.ContatoDTO;
import br.com.mastertech.contatos.models.Contato;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ContatoConverter {

    public ContatoDTO toContatoDTO (Contato contato){
        ContatoDTO contatoDTO = new ContatoDTO();
        contatoDTO.setNome(contato.getNome());
        contatoDTO.setNumero(contato.getNumero());
        return contatoDTO;
    }

    public List<ContatoDTO> toContatosDTO (List<Contato> listaContatos){
        List<ContatoDTO> listaContatosDTO = new ArrayList<>();

        for(Contato contato : listaContatos){
            ContatoDTO contatoDTO = new ContatoDTO();
            contatoDTO.setNome(contato.getNome());
            contatoDTO.setNumero(contato.getNumero());
            listaContatosDTO.add(contatoDTO);
        }

        return listaContatosDTO;
    }

}
