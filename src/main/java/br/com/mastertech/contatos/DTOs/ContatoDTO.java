package br.com.mastertech.contatos.DTOs;

public class ContatoDTO {

    private String nome;

    private int numero;

    public ContatoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
